<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object has been created as part of POP release. This object will hold fishing licence specific information.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field will link the associated contact with the fishing licence. This field has been created as part of POP.</description>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Fishing Licences</relationshipLabel>
        <relationshipName>fishingLicences</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field has been created as part of POP. This field will hold the product associated with a fishing licence.</description>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Fishing Licences</relationshipLabel>
        <relationshipName>fishingLicenceProduct</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>TransactionReferenceNumber__c</fullName>
        <externalId>false</externalId>
        <label>TransactionReferenceNumber</label>
        <length>128</length>
        <referenceTo>transactions__x</referenceTo>
        <relationshipLabel>Fishing Licences</relationshipLabel>
        <relationshipName>FishingLicences</relationshipName>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>ExternalLookup</type>
    </fields>
    <fields>
        <fullName>fishingLicenceNumber__c</fullName>
        <caseSensitive>true</caseSensitive>
        <description>This field has been created as part of POP. This will hold the licence number provided by the API.</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Fishing Licence Number</label>
        <length>80</length>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>licenceExpiryDateTime__c</fullName>
        <description>This field has been created as part of POP.</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Licence Expiry Date Time</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>licenceStartDateTime__c</fullName>
        <description>This field has been created as part of POP.</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Licence Start Date Time</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>receivedDateTime__c</fullName>
        <description>This field has been created as part of POP.</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Received Date Time</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <label>Fishing Licence</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>fishingLicenceNumber__c</columns>
        <columns>Contact__c</columns>
        <columns>licenceStartDateTime__c</columns>
        <columns>licenceExpiryDateTime__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>FishingLicence Name</label>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Fishing Licences</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>fishingLicenceNumber__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Contact__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>licenceStartDateTime__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>licenceExpiryDateTime__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATED_DATE</customTabListAdditionalFields>
        <searchResultsAdditionalFields>fishingLicenceNumber__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Contact__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>licenceExpiryDateTime__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>licenceStartDateTime__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATED_DATE</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
